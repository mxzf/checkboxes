# Changelog

## 0.1.0 - Initial release

Initial release of the module

### Features

* The ability to use an `@checkbox(id)` enricher, with a unique `id`, in order to make a checkbox in a document.  

## 