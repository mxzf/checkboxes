# Checkboxes

This module adds an `@checkbox(id)` enricher to Foundry, where `id` is a unique identifier for the individual checkbox in that sheet.  It will become a checkbox when the document is enriched and rendered, with the ability to store the value properly.

## Installation

Module manifest: https://gitlab.com/mxzf/checkboxes/-/releases/permalink/latest/downloads/module.json

## Warnings

1. You need to have some kind of identifier for each checkbox in order for it to tie the data back to the various spots for checkboxes.

2. The IDs need to be unique in each document.  Multiple of the same ID will almost certainly cause weird HTML rendering issues.