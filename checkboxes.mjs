Hooks.on('setup', () => {
    CONFIG.TextEditor.enrichers.push({
        pattern: /@checkbox\((.+)\)/gi,
        enricher: async (match, options) => {
            const checkbox = document.createElement('input')
            checkbox.type = 'checkbox'
            checkbox.name = `flags.checkboxes.${match[1]}`
            // This if statement is here because apparently browsers are really stupid about `.checked = true`
            if (options.relativeTo.flags?.checkboxes?.[match[1]]) checkbox.setAttribute('checked',true)
            return checkbox
        }
    })
})